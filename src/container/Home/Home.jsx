//libraries
import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

//pages
import Product from "../pages/Product/Product";
import LifeCycleComp from "../pages/LifeCycleComp/LifeCycleComp";
import BlogPost from "../pages/BlogPost/BlogPost";
import YoutubeCompPage from "../pages/YouubeCompPage/YoutubeCompPage";
import DetailPost from "../pages/BlogPost/DetailPost/DetailPost";
import Hooks from "../pages/Hooks/Hooks";

//style
import "./Home.css";

export default class Home extends Component {
  state = {
    showComponent: true
  };

  componentDidMount() {
    // setTimeout( () => {
    //     this.setState({
    //         showComponent : false
    //     })
    // }, 15000)
  }

  render() {
    return (
      <Router>
        <div className="navigation">
          <Link to="/">Blog Post</Link>
          <Link to="/product">Product</Link>
          <Link to="/lifecycle">Life Cycle</Link>
          <Link to="/youtube-component">Youtube Component</Link>
          <Link to="/hooks">Hooks</Link>
        </div>
        <Route path="/" exact component={BlogPost} />
        <Route path="/detail-post/:postID" component={DetailPost} />
        <Route path="/product" component={Product} />
        <Route path="/lifecycle" component={LifeCycleComp} />
        <Route path="/youtube-component" component={YoutubeCompPage} />
        <Route path="/hooks" component={Hooks} />
      </Router>
    );
  }
}
