import React, { Component } from "react";
import Counter from "./Counter";

export default class CardProduct extends Component {
  render() {
    return (
      <div className="card">
        <div className="img-thumb-prod">
          <img
            src="https://s3-ap-southeast-1.amazonaws.com/etanee-images/product/bsl_1kg_pack15.jpg"
            alt=""
          />
        </div>
        <p className="product-title">
          Paha Ayam Frozen Tanpa Tulang [1 Carton - 15 Pack x 1Kg]
        </p>
        <p className="product-price">Rp 705,000</p>
        <Counter onCounterChange={value => this.props.onCounterChange(value)} />
      </div>
    );
  }
}
