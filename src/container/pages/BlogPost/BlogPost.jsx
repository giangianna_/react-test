import React, { Component, Fragment } from "react";
import "./BlogPost.css";
import Post from "../../../components/Post/Post";
// import axios from "axios";
import API from "../../../services";

export default class BlogPost extends Component {
  state = {
    post: [],
    formBlogPost: {
      id: 1,
      userId: 1,
      title: "",
      body: ""
    },
    isUpdate: false,
    comments: []
  };

  getPostAPI = () => {
    API.getNewsBlog().then(result => {
      this.setState({
        post: result
      });
    });
    API.getComments().then(result => {
      this.setState({
        comments: result
      });
    });
  };

  postDataToAPI = () => {
    API.postNewsBlog(this.state.formBlogPost).then(res => {
      this.getPostAPI();
      this.setState({
        formBlogPost: {
          id: 1,
          userId: 1,
          title: "",
          body: ""
        }
      });
    });
  };

  putDataToAPI = () => {
    API.updateNewsBlog(
      this.state.formBlogPost,
      this.state.formBlogPost.id
    ).then(res => {
      this.getPostAPI();
      this.setState({
        isUpdate: false,
        formBlogPost: {
          id: 1,
          userId: 1,
          title: "",
          body: ""
        }
      });
    });
  };

  handleRemove = data => {
    API.deleteNewsBlog(data).then(res => {
      this.getPostAPI();
    });
  };

  handleUpdate = data => {
    console.log(data);
    this.setState({
      formBlogPost: data,
      isUpdate: true
    });
  };

  handleFormChange = event => {
    // console.log('form change', event.target)
    let formBlogPostNew = { ...this.state.formBlogPost };
    let timestamp = new Date().getTime();
    if (!this.state.isUpdate) {
      formBlogPostNew["id"] = timestamp;
    }
    formBlogPostNew[event.target.name] = event.target.value;

    this.setState({
      formBlogPost: formBlogPostNew
    });
  };

  handleSubmit = () => {
    if (this.state.isUpdate) {
      this.putDataToAPI();
    } else {
      this.postDataToAPI();
    }
  };

  handleDetail = id => {
    this.props.history.push(`/detail-post/${id}`);
  };

  componentDidMount() {
    // menggunakan fetch
    // fetch('https://jsonplaceholder.typicode.com/posts')
    // .then(response => response.json())
    // .then(json => {
    //     this.setState({
    //         post: json
    //     })
    // })
    this.getPostAPI();
  }

  render() {
    return (
      <Fragment>
        <p className="section-title">Blog Post</p>
        <hr />
        <div className="form-add-post">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            value={this.state.formBlogPost.title}
            name="title"
            placeholder="add title"
            onChange={this.handleFormChange}
          />
          <label htmlFor="body">Blog Content</label>
          <textarea
            name="body"
            value={this.state.formBlogPost.body}
            id="body"
            cols="30"
            rows="10"
            placeholder="add blog content"
            onChange={this.handleFormChange}
          />
          <button className="btn-submit" onClick={this.handleSubmit}>
            Simpan
          </button>
        </div>
        {this.state.comments.map(comment => {
          return (
            <p key={comment.id}>
              {comment.name} - {comment.email}
            </p>
          );
        })}
        {this.state.post.map(post => {
          return (
            <Post
              key={post.id}
              data={post}
              remove={this.handleRemove}
              update={this.handleUpdate}
              goDetail={this.handleDetail}
            />
          );
        })}
      </Fragment>
    );
  }
}
