import React from "react";
import "./YoutubeComp.css";
import naruto from "../../img/naruto.jpg";

const YoutubeComp = props => {
  return (
    <div className="youtube-wrapper">
      <div className="img-thumb">
        <img src={naruto} alt="" />
        <p className="time">{props.time}</p>
      </div>
      <p className="title">{props.title}</p>
      <p className="desc">{props.desc}</p>
    </div>
  );
};

YoutubeComp.defaultProps = {
  time: "00.00",
  title: "Title Here",
  desc: "Desc Here"
};

export default YoutubeComp;
